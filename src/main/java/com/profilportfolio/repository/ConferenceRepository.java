package com.profilportfolio.repository;

import com.profilportfolio.model.ConferenceEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Repository
public interface ConferenceRepository extends JpaRepository<ConferenceEntity, Integer> {


    /**
     * OBTENIR TOUTES LES CONFERENCE PAR L'ID DU PROFIL
     * @param profilId
     * @return
     */
    List<ConferenceEntity> findAllByProfilId(Integer profilId);


    /**
     * VERIFIE SI LA CONFERENCE EXISTE DEJA
     * @param conferenceName
     * @return
     */
    @Query(value = "SELECT COUNT(conference) > 0 FROM conference WHERE conference.name= :conferenceName", nativeQuery = true)
    Boolean conferenceExistsByName(@Param("conferenceName") String conferenceName);


    /**
     * SUPPRIMER LA CONFERENCE PAR SON ID
     * @param conferenceId
     * @return
     */
    @Modifying @Transactional
    @Query(value = "DELETE FROM conference" +
            " WHERE conference.id= :conferenceId", nativeQuery = true)
    int deleteConferenceById(@Param("conferenceId") Integer conferenceId);

}
