package com.profilportfolio.service.dto;

import lombok.*;

@Getter @Setter @Builder @AllArgsConstructor @NoArgsConstructor
public class VideoDTO {

    private Integer id;

    private String title;

    private String videoUrl;
}
