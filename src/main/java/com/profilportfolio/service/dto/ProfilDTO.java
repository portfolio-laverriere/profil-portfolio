package com.profilportfolio.service.dto;

import lombok.*;

import java.sql.Timestamp;
import java.util.List;

@Getter @Setter @Builder @AllArgsConstructor @NoArgsConstructor
public class ProfilDTO {

    private Integer id;

    private String lastName;

    private String firstName;

    private String adress;

    private Timestamp dateOfBirth;

    private String phoneNumber;

    private String email;
    
    private List<CompetenceDTO> competenceDTOList;
    
    private List<ConferenceDTO> conferenceDTOList;
    
    private List<DiplomaDTO> diplomaDTOList;
    
    private List<ExperienceDTO> experienceDTOList;
    
    private List<PointOfInterestDTO> pointOfInterestDTOList;
    
    private List<SocialNetworkDTO> socialNetworkDTOList;
    
}
