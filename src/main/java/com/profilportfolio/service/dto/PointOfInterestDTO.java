package com.profilportfolio.service.dto;

import lombok.*;

import java.util.List;

@Getter @Setter @Builder @AllArgsConstructor @NoArgsConstructor
public class PointOfInterestDTO {

    private Integer id;

    private String name;

    private String paragraph;

    private Integer profilId;

    private List<TagDTO> tagDTOList;

}
