package com.profilportfolio.service.mapper;

import com.profilportfolio.model.DiplomaEntity;
import com.profilportfolio.service.dto.DiplomaDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import java.util.List;

@Mapper
public interface DiplomaMapper {

    DiplomaMapper INSTANCE = Mappers.getMapper( DiplomaMapper.class );

    DiplomaDTO toDTO ( DiplomaEntity diplomaEntity );

    List<DiplomaDTO> toDTOList ( List<DiplomaEntity> diplomaEntityList );

    DiplomaEntity toEntity ( DiplomaDTO diplomaDTO );

    List<DiplomaEntity> toEntityList ( List<DiplomaDTO> diplomaDTOList );

}
