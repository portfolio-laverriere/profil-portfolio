package com.profilportfolio.service.mapper;

import com.profilportfolio.model.ProfilEntity;
import com.profilportfolio.service.dto.ProfilDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import java.util.List;

@Mapper
public interface ProfilMapper {

    ProfilMapper INSTANCE = Mappers.getMapper( ProfilMapper.class );

    ProfilDTO toDTO (ProfilEntity profilEntity );

    List<ProfilDTO> toDTOList ( List<ProfilEntity> profilEntityList );

    ProfilEntity toEntity ( ProfilDTO profilDTO );

    List<ProfilEntity> toEntityList ( List<ProfilDTO> profilDTOList );

}
