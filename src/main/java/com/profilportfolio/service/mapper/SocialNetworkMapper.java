package com.profilportfolio.service.mapper;

import com.profilportfolio.model.SocialNetworkEntity;
import com.profilportfolio.service.dto.SocialNetworkDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import java.util.List;

@Mapper
public interface SocialNetworkMapper {

    SocialNetworkMapper INSTANCE = Mappers.getMapper( SocialNetworkMapper.class );

    SocialNetworkDTO toDTO ( SocialNetworkEntity socialNetworkEntity );

    List<SocialNetworkDTO> toDTOList ( List<SocialNetworkEntity> socialNetworkEntityList );

    SocialNetworkEntity toEntity ( SocialNetworkDTO socialNetworkDTO );

    List<SocialNetworkEntity> toEntityList ( List<SocialNetworkDTO> socialNetworkDTOList );

}
