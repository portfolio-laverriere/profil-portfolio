package com.profilportfolio.service.mapper;

import com.profilportfolio.model.TagEntity;
import com.profilportfolio.service.dto.TagDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import java.util.List;

@Mapper
public interface TagMapper {

    TagMapper INSTANCE = Mappers.getMapper( TagMapper.class );

    TagDTO toDTO ( TagEntity tagEntity );

    List<TagDTO> toDTOList ( List<TagEntity> tagEntityList );

    TagEntity toEntity ( TagDTO tagDTO );

    List<TagEntity> toEntityList ( List<TagDTO> tagDTOList );

}
