package com.profilportfolio.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class ProfilConflictException extends RuntimeException {

    public ProfilConflictException(String s) {
        super(s);
    }
}
