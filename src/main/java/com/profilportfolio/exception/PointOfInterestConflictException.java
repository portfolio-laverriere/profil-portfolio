package com.profilportfolio.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class PointOfInterestConflictException extends RuntimeException {

    public PointOfInterestConflictException(String s) {
        super(s);
    }

}
