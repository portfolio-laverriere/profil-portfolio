package com.profilportfolio.model;

import lombok.*;
import javax.persistence.*;

@Getter @Setter @Builder @AllArgsConstructor @NoArgsConstructor
@Entity @Table(name = "competence")
public class CompetenceEntity {

    @Id @GeneratedValue
    private Integer id;

    private String name;

    private Integer level;

    @Column(name = "number_of_practice_months")
    private Integer numberOfPracticeMonths;

    @Column(name = "profil_id")
    private Integer profilId;

}
