package com.profilportfolio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients("com.profilportfolio")
public class ProfilPortfolioApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProfilPortfolioApplication.class, args);
	}

}
