package com.profilportfolio.controller;

import com.profilportfolio.exception.*;
import com.profilportfolio.proxy.MicroserviceMediaProxy;
import com.profilportfolio.repository.ExperienceRepository;
import com.profilportfolio.repository.ProfilRepository;
import com.profilportfolio.service.dto.ExperienceDTO;
import com.profilportfolio.service.mapper.ExperienceMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Api( description = "API POUR LES OPERATIONS CRUD SUR LES EXPERIENCES")
@RestController
public class ExperienceController {

    private static final Logger logger = LoggerFactory.getLogger(ExperienceController.class);

    @Autowired
    private ExperienceRepository experienceRepository;

    @Autowired
    private ProfilRepository profilRepository;

    @Autowired
    private MicroserviceMediaProxy microserviceMediaProxy;


                                                /* ===================================== */
                                                /* ================ GET ================ */
                                                /* ===================================== */

    /* --------------------------------------- GET ALL EXPERIENCE BY PROFIL ---------------------------------------- */
   @ApiOperation(value = "GET ALL EXPERIENCE BY PROFIL")
    @GetMapping(value = "/profil/experience/get-all-experience/{profilId}")
    public List<ExperienceDTO> getAllExperienceByProfil(@PathVariable Integer profilId) {

       List<ExperienceDTO> experienceDTOList = new ArrayList<>();

       /**
        * JE VERIFIE SI LE PROFIL EXISTE
        */
       Boolean profilExists = profilRepository.existsById(profilId);

       if (profilExists) {

           try {

               /**
                * OBTENIR TOUTES LES EXPERIENCES PAR L'ID PROFIL
                * @see ExperienceRepository#findAllByProfilId(Integer)
                */
               experienceDTOList = ExperienceMapper.INSTANCE.toDTOList(experienceRepository.findAllByProfilId(profilId));

               /**
                * TRIER LES EXPERIENCES PAR ORDRE DECROISSANT EN FONCTION DE LEURS DATES
                */
               Collections.sort(experienceDTOList, new Comparator<ExperienceDTO>() {
                   @Override
                   public int compare(ExperienceDTO o1, ExperienceDTO o2) {
                       return o2.getCommencementDate().compareTo(o1.getCommencementDate());
                   }
               });

               for (ExperienceDTO experienceDTO : experienceDTOList) {

                   /**
                    * OBTENIR TOUTES LES IMAGES PAR L'ID EXPERIENCE
                    * @see MicroserviceMediaProxy#getListImageByExperience(Integer)
                    */
                   experienceDTO.setImageDTOList(microserviceMediaProxy.getListImageByExperience(experienceDTO.getId()));
               }

           } catch (Exception pEX) {
               logger.error(String.valueOf(pEX));
           }

       } else {
           throw new ProfilNotFoundException("Profil not found for the ID : " + profilId +
                   " - Cannot retrieve experience related to the profil");
       }
       return experienceDTOList;
    }


                                            /* ===================================== */
                                            /* ================ ADD ================ */
                                            /* ===================================== */

    /* ------------------------------------------ ADD EXPERIENCE ------------------------------------------ */
    @ApiOperation(value = "ADD EXPERIENCE FOR PROFIL")
    @PostMapping(value = "/profil/experience/add-experience/{profilId}")
    public List<ExperienceDTO> addExperienceForProfil(@RequestBody List<ExperienceDTO> experienceDTOList,
                                                      @PathVariable Integer profilId) {

        /**
         * JE VERIFIE SI LE PROFIL EXISTE
         */
        Boolean profilExists = profilRepository.existsById(profilId);
        
        if (profilExists) {

            for (ExperienceDTO experienceDTO : experienceDTOList) {

                /** J'AJOUTE A EXPERIENCE L'ID DU PROFIL AUQUEL CELUI-CI EST LIE */
                experienceDTO.setProfilId(profilId);
                
                /** @see ExperienceRepository#experienceExistsByTitle(String) */
                Boolean experienceExistsByTitle = experienceRepository.experienceExistsByTitle(experienceDTO.getTitle());

                if (!experienceExistsByTitle) {

                    /** J'AJOUTE EXPERIENCE, PUIS JE RECUPERE SON IDENTIFIANT */
                    experienceDTO.setId(ExperienceMapper.INSTANCE.toDTO(
                            experienceRepository.save(ExperienceMapper.INSTANCE.toEntity(experienceDTO))).getId());
                    
                } else  {
                    throw new ExperienceConflictException("The experience for the following name '" + experienceDTO.getTitle() +
                            "' has already been added");
                }
            }
            
        } else {
            throw new ProfilNotFoundException("Profil not found for the ID : " + profilId +
                    " - Cannot add experience related to the profile");
        }
        return experienceDTOList;
    }


                                        /* ===================================== */
                                        /* ============== UPDATE =============== */
                                        /* ===================================== */

    /* -------------------------------------------- UP EXPERIENCE -------------------------------------------- */
    @ApiOperation(value = "UP EXPERIENCE")
    @PutMapping(value = "/profil/experience/up-experience")
    public ExperienceDTO upExperience(@RequestBody ExperienceDTO experienceDTO) {

        /**
         * JE VERIFIE SI L'EXPERIENCE EXISTE
         */
        Boolean experienceExists = experienceRepository.existsById(experienceDTO.getId());

      if (experienceExists) {

          try {

              experienceDTO = ExperienceMapper.INSTANCE.toDTO(experienceRepository.save(
                      ExperienceMapper.INSTANCE.toEntity(experienceDTO)));

          } catch (Exception pEX) {
              logger.error("{}", String.valueOf(pEX));
          }

      } else {
          logger.error("Update failure - experience : '" + experienceDTO.getTitle() + "' - Is not found");
          throw new ExperienceNotFoundException("Update failure - experience : '" + experienceDTO.getTitle() +
                  "' - Is not found");
      }
        return experienceDTO;
    }


                                        /* ======================================= */
                                        /* =============== DELETE  =============== */
                                        /* ======================================= */

    /* ------------------------------------- DEL EXPERIENCE ------------------------------------- */
    @ApiOperation(value = "DEL EXPERIENCE")
    @DeleteMapping(value = "/profil/experience/del-experience/{experienceId}")
    public String delExperience(@PathVariable Integer experienceId) {

        String massageStatus = "";

        try {

            /** @see ExperienceRepository#deleteExperienceById(Integer) */
            Integer check = experienceRepository.deleteExperienceById(experienceId);

            if (check == 0) {
                massageStatus = "Failed deletion - experience not found";
            } else {
                massageStatus = "Successful deletion of the experience";
            }
        } catch (Exception pEX) {
            logger.error(String.valueOf(pEX));
        }
        return massageStatus;
    }

}
