package com.profilportfolio.controller;

import com.profilportfolio.exception.*;
import com.profilportfolio.model.TagEntity;
import com.profilportfolio.repository.PointOfInterestRepository;
import com.profilportfolio.repository.ProfilRepository;
import com.profilportfolio.repository.TagRepository;
import com.profilportfolio.service.dto.PointOfInterestDTO;
import com.profilportfolio.service.dto.TagDTO;
import com.profilportfolio.service.mapper.PointOfInterestMapper;
import com.profilportfolio.service.mapper.TagMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@Api( description = "API POUR LES OPERATIONS CRUD SUR LES POINT OF INTEREST")
@RestController
public class PointOfInterestController {

    private static final Logger logger = LoggerFactory.getLogger(PointOfInterestController.class);

    @Autowired
    private PointOfInterestRepository pointOfInterestRepository;

    @Autowired
    private ProfilRepository profilRepository;
    
    @Autowired
    private TagRepository tagRepository;


                                    /* ===================================== */
                                    /* ================ GET ================ */
                                    /* ===================================== */

    /* -------------------------------- GET ALL POINT OF INTEREST BY PROFIL ------------------------------------ */
    @ApiOperation(value = "GET ALL POINT OF INTEREST BY PROFIL")
    @GetMapping(value = "/profil/pointofinterest/get-all-pointofinterest/{profilId}")
    public List<PointOfInterestDTO> getAllPointOfInterestByProfil(@PathVariable Integer profilId) {

        List<PointOfInterestDTO> pointOfInterestDTOList = new ArrayList<>();

        /**
         * JE VERIFIE SI LE PROFIL EXISTE
         */
        Boolean profilExists = profilRepository.existsById(profilId);

        if (profilExists) {
            
            try {

                /**
                 * OBTENIR TOUTES LES POINT OF INTEREST PAR L'ID PROFIL
                 * @see PointOfInterestRepository#findAllByProfilId(Integer)
                 */
                pointOfInterestDTOList = PointOfInterestMapper.INSTANCE.toDTOList(
                        pointOfInterestRepository.findAllByProfilId(profilId));

                for (PointOfInterestDTO pointOfInterestDTO : pointOfInterestDTOList) {

                    /**
                     * OBTENIR TOUTES LES TAGS PAR L'ID POINT OF INTEREST
                     * @see TagRepository#findAllTagByPointOfInterest(Integer)
                     */
                    pointOfInterestDTO.setTagDTOList(TagMapper.INSTANCE.toDTOList(
                            tagRepository.findAllTagByPointOfInterest(pointOfInterestDTO.getId())));
                }

            } catch (Exception pEX) {
                logger.error(String.valueOf(pEX));
            }

        } else {
            throw new ProfilNotFoundException("Profil not found for the ID : " + profilId +
                    " - Cannot retrieve diploma related to the profil");
        }
        return pointOfInterestDTOList;
    }


                                    /* ===================================== */
                                    /* ================ ADD ================ */
                                    /* ===================================== */

    /* ------------------------------------------ ADD POINT OF INTEREST ------------------------------------------ */
    @ApiOperation(value = "ADD POINT OF INTEREST FOR PROFIL")
    @PostMapping(value = "/profil/pointofinterest/add-pointofinterest/{profilId}")
    public List<PointOfInterestDTO> addPointOfInterestForProfil(@RequestBody List<PointOfInterestDTO> pointOfInterestDTOList,
                                                      @PathVariable Integer profilId) {

        /**
         * JE VERIFIE SI LE PROFIL EXISTE
         */
        Boolean profilExists = profilRepository.existsById(profilId);
        
        if (profilExists) {

            for (PointOfInterestDTO pointOfInterestDTO : pointOfInterestDTOList) {

                /** J'AJOUTE A POINT OF INTEREST L'ID DU PROFIL AUQUEL CELUI-CI EST LIE */
                pointOfInterestDTO.setProfilId(profilId);
                
                /** @see PointOfInterestRepository#pointOfInterestExistsByName(String) */
                Boolean pointOfInterestExistsByName = pointOfInterestRepository.pointOfInterestExistsByName(pointOfInterestDTO.getName());

                if (!pointOfInterestExistsByName) {
                    
                    /** J'AJOUTE POINT OF INTEREST, PUIS JE RECUPERE SON IDENTIFIANT */
                    pointOfInterestDTO.setId(PointOfInterestMapper.INSTANCE.toDTO(
                            pointOfInterestRepository.save(PointOfInterestMapper.INSTANCE.toEntity(pointOfInterestDTO))).getId());

                } else {
                    throw new PointOfInterestConflictException("The point of interest for the following name '" + pointOfInterestDTO.getName() +
                            "' has already been added");
                }
            }

        } else {
            throw new ProfilNotFoundException("Profil not found for the ID : " + profilId +
                    " - Cannot add point of interest related to the profile");
        }
        return pointOfInterestDTOList;
    }

    /* ------------------------------------- ADD TAG FOR POINT OF INTEREST ------------------------------------- */
    @ApiOperation(value = "ADD TAG FOR POINT OF INTEREST")
    @PostMapping(value = "/profil/pointofinterest/tag/add-tag/{pointofinterestId}")
    public List<TagDTO> addTagPointOfInterest(@RequestBody List<TagDTO> tagDTOList,
                                              @PathVariable Integer pointofinterestId) {

        for (TagDTO tagDTO : tagDTOList) {

            /**
             * JE VERIFIE SI LE TAG EST DEJA PRESENTE DANS LA TABLE
             * @see TagRepository#findByTagPath
             */
            TagEntity tagEntity = tagRepository.findByTagPath(tagDTO.getTagPath());

            /**
             * SI LE TAG EST DEJA PRESENT DANS LA TABLE, JE JOINS CELUI-CI A LA TABLE POINT OF INTEREST DANS
             * LA TABLE DE JOINTURE "TAG_POINTOFINTEREST", SINON J'AJOUTE LA NOUVEAU TAG ET JE CREE LA JOINTURE
             */
            if (tagEntity != null) {

                /**
                 * JE VERIFIE SI POINT OF INTEREST ET TAG SON DEJA LIES
                 * @see TagRepository#tagPointOfInterestExists(Integer, Integer)
                 */
                Boolean tagPointOfInterestExists = tagRepository.tagPointOfInterestExists(tagEntity.getId(), pointofinterestId);

                if (!tagPointOfInterestExists) {

                    try {

                        /** @see TagRepository#joinTagToThePointOfInterest(Integer, Integer) */
                        tagRepository.joinTagToThePointOfInterest(tagEntity.getId(), pointofinterestId);
                        tagDTO.setId(tagEntity.getId());

                    } catch (DataIntegrityViolationException pEX) {
                        throw new PointOfInterestNotFoundException("Point of interest not found for the ID : " + pointofinterestId +
                                " - Impossible to link the tag to the point of interest");
                    } catch (Exception pEX) {
                        logger.error(String.valueOf(pEX));
                    }

                } else {
                    throw new PointOfInterestConflictException("The tag is already linked to the point of interest : " + pointofinterestId);
                }

            } else {

                /**
                 * JE VERIFIE SI LE POINT OF INTEREST  EXISTE
                 */
                Boolean pointofinterestExists = pointOfInterestRepository.existsById(pointofinterestId);

                if (pointofinterestExists) {

                    /** J'AJOUTE LE TAG, PUIS JE RECUPERE SON IDENTIFIANT */
                    tagDTO.setId(TagMapper.INSTANCE.toDTO(
                            tagRepository.save(TagMapper.INSTANCE.toEntity(tagDTO))).getId());

                    /** @see TagRepository#joinTagToThePointOfInterest(Integer, Integer) */
                    tagRepository.joinTagToThePointOfInterest(tagDTO.getId(), pointofinterestId);

                } else {
                    throw new PointOfInterestNotFoundException("Point of interest not found for the ID : " + pointofinterestId +
                            " - Impossible to add the tag and link it to the point of interest");
                }
            }
        }
        return tagDTOList;
    }


                                        /* ===================================== */
                                        /* ============== UPDATE =============== */
                                        /* ===================================== */

    /* -------------------------------------------- UP POINT OF INTEREST -------------------------------------------- */
    @ApiOperation(value = "UP POINT OF INTEREST")
    @PutMapping(value = "/profil/pointofinterest/up-pointofinterest")
    public PointOfInterestDTO upPointOfInterest(@RequestBody PointOfInterestDTO pointOfInterestDTO) {

        /**
         * JE VERIFIE SI LA POINT OF INTEREST EXISTE
         */
        Boolean pointOfInterestExists= pointOfInterestRepository.existsById(pointOfInterestDTO.getId());

        if (pointOfInterestExists) {

            try {

                pointOfInterestDTO = PointOfInterestMapper.INSTANCE.toDTO(
                        pointOfInterestRepository.save(PointOfInterestMapper.INSTANCE.toEntity(pointOfInterestDTO)));

            } catch (Exception pEX) {
                logger.error("{}", String.valueOf(pEX));
            }

        } else {
            logger.error("Update failure - point of interest : '" + pointOfInterestDTO.getName() + "' - Is not found");
            throw new PointOfInterestNotFoundException("Update failure - point of interest : '" + pointOfInterestDTO.getName() +
                    "' - Is not found");
        }
        return pointOfInterestDTO;
    }


                                        /* ======================================= */
                                        /* =============== DELETE  =============== */
                                        /* ======================================= */

    /* ------------------------------------- DEL POINT OF INTEREST ------------------------------------- */
    @ApiOperation(value = "DEL POINT OF INTEREST")
    @DeleteMapping(value = "/profil/pointofinterest/del-pointofinterest/{pointofinterestId}")
    public String delPointOfInterest(@PathVariable Integer pointofinterestId) {

        String massageStatus = "";

        try {

            /** @see PointOfInterestRepository#deletePointOfInterestById(Integer) */
            Integer check = pointOfInterestRepository.deletePointOfInterestById(pointofinterestId);

            if (check == 0) {
                massageStatus = "Failed deletion - point of interest not found";
            } else {
                massageStatus = "Successful deletion of the point of interest";
            }
        } catch (Exception pEX) {
            logger.error(String.valueOf(pEX));
        }
        return massageStatus;
    }

    /* ------------------------------------- DEL TAG BY POINT OF INTEREST ------------------------------------- */
    @ApiOperation(value = "DEL TAG BY POINT OF INTEREST")
    @DeleteMapping(value = "/profil/pointofinterest/tag/del-tag/{tagId}/{pointofinterestId}")
    public String delTagByPointOfInterest(@PathVariable Integer tagId, @PathVariable Integer pointofinterestId) {

        String massageStatus = "";

        try {

        /** @see TagRepository#deleteTagForPointOfInterest(Integer, Integer) */
            Integer check = tagRepository.deleteTagForPointOfInterest(tagId, pointofinterestId);

            if (check == 0) {
                massageStatus = "Failed deletion - tag not found";
            } else {
                massageStatus = "Successful deletion of the tag";
            }

        } catch (Exception pEX) {
            logger.error(String.valueOf(pEX));
        }
        return massageStatus;
    }

}
