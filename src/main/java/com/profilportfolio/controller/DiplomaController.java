package com.profilportfolio.controller;

import com.profilportfolio.exception.DiplomaConflictException;
import com.profilportfolio.exception.DiplomaNotFoundException;
import com.profilportfolio.exception.ProfilNotFoundException;
import com.profilportfolio.proxy.MicroserviceMediaProxy;
import com.profilportfolio.repository.DiplomaRepository;
import com.profilportfolio.repository.ProfilRepository;
import com.profilportfolio.service.dto.DiplomaDTO;
import com.profilportfolio.service.mapper.DiplomaMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Api( description = "API POUR LES OPERATIONS CRUD SUR LES DIPLOMAS")
@RestController
public class DiplomaController {

    private static final Logger logger = LoggerFactory.getLogger(DiplomaController.class);

    @Autowired
    private DiplomaRepository diplomaRepository;

    @Autowired
    private ProfilRepository profilRepository;

    @Autowired
    private MicroserviceMediaProxy microserviceMediaProxy;


                                        /* ===================================== */
                                        /* ================ GET ================ */
                                        /* ===================================== */

    /* --------------------------------------- GET ALL DIPLOMA BY PROFIL ---------------------------------------- */
    @ApiOperation(value = "GET ALL DIPLOMA BY PROFIL")
    @GetMapping(value = "/profil/diploma/get-all-diploma/{profilId}")
    public List<DiplomaDTO> getAllDiplomaByProfil(@PathVariable Integer profilId) {

        List<DiplomaDTO> diplomaDTOList = new ArrayList<>();

        /**
         * JE VERIFIE SI LE PROFIL EXISTE
         */
        Boolean profilExists = profilRepository.existsById(profilId);

        if (profilExists) {

            try {

                /**
                 * OBTENIR TOUTES LES DIPLOMES PAR L'ID PROFIL
                 * @see DiplomaRepository#findAllByProfilId(Integer)
                 */
                diplomaDTOList = DiplomaMapper.INSTANCE.toDTOList(diplomaRepository.findAllByProfilId(profilId));

                /**
                 * TRIER LES DIPLOMES PAR ORDRE DECROISSANT EN FONCTION DE LEURS DATES
                 */
                Collections.sort(diplomaDTOList, new Comparator<DiplomaDTO>() {
                    @Override
                    public int compare(DiplomaDTO o1, DiplomaDTO o2) {
                        return o2.getObtaining().compareTo(o1.getObtaining());
                    }
                });

                for (DiplomaDTO diplomaDTO : diplomaDTOList) {

                    /**
                     * OBTENIR TOUTES LES IMAGES PAR L'ID DIPLOMA
                     * @see MicroserviceMediaProxy#getListImageByDiploma(Integer)
                     */
                    diplomaDTO.setImageDTOList(microserviceMediaProxy.getListImageByDiploma(diplomaDTO.getId()));
                }

            } catch (Exception pEX) {
                logger.error(String.valueOf(pEX));
            }

        } else {
            throw new ProfilNotFoundException("Profil not found for the ID : " + profilId +
                    " - Cannot retrieve diploma related to the profil");
        }
        return diplomaDTOList;
    }


                                        /* ===================================== */
                                        /* ================ ADD ================ */
                                        /* ===================================== */

    /* ------------------------------------------ ADD DIPLOMA ------------------------------------------ */
    @ApiOperation(value = "ADD DIPLOMA FOR PROFIL")
    @PostMapping(value = "/profil/diploma/add-diploma/{profilId}")
    public List<DiplomaDTO> addDiplomaForProfil(@RequestBody List<DiplomaDTO> diplomaDTOList,
                                                @PathVariable Integer profilId) {

        /**
         * JE VERIFIE SI LE PROFIL EXISTE
         */
        Boolean profilExists = profilRepository.existsById(profilId);

        if (profilExists) {

            for (DiplomaDTO diplomaDTO : diplomaDTOList) {

                /** J'AJOUTE A DIPLOMA L'ID DU PROFIL AUQUEL CELUI-CI EST LIE */
                diplomaDTO.setProfilId(profilId);

                /** @see DiplomaRepository#diplomaExistsByTitle(String) */
                Boolean diplomaExistsByTitle = diplomaRepository.diplomaExistsByTitle(diplomaDTO.getTitle());

                if (!diplomaExistsByTitle) {

                    /** J'AJOUTE DIPLOMA, PUIS JE RECUPERE SON IDENTIFIANT */
                    diplomaDTO.setId(DiplomaMapper.INSTANCE.toDTO(
                            diplomaRepository.save(DiplomaMapper.INSTANCE.toEntity(diplomaDTO))).getId());

                } else {
                    throw new DiplomaConflictException("The diploma for the following name '" + diplomaDTO.getTitle() +
                            "' has already been added");
                }
            }

        } else {
            throw new ProfilNotFoundException("Profil not found for the ID : " + profilId +
                    " - Cannot add diploma related to the profile");
        }
        return diplomaDTOList;
    }


                                        /* ===================================== */
                                        /* ============== UPDATE =============== */
                                        /* ===================================== */

    /* ---------------------------------------------- UP DIPLOMA ---------------------------------------------- */
    @ApiOperation(value = "UP DIPLOMA")
    @PutMapping(value = "/profil/diploma/up-diploma")
    public DiplomaDTO upDiploma(@RequestBody DiplomaDTO diplomaDTO) {

        /**
         * JE VERIFIE SI LA DIPLOMA EXISTE
         */
        Boolean diplomaExists = diplomaRepository.existsById(diplomaDTO.getId());

        if (diplomaExists) {

            try {

                diplomaDTO = DiplomaMapper.INSTANCE.toDTO(diplomaRepository.save(
                        DiplomaMapper.INSTANCE.toEntity(diplomaDTO)));

            } catch (Exception pEX) {
                logger.error("{}", String.valueOf(pEX));
            }

        } else {
            logger.error("Update failure - diploma : '" + diplomaDTO.getTitle() + "' - Is not found");
            throw new DiplomaNotFoundException("Update failure - diploma : '" + diplomaDTO.getTitle() +
                    "' - Is not found");
        }
        return diplomaDTO;
    }


                                        /* ======================================= */
                                        /* =============== DELETE  =============== */
                                        /* ======================================= */

    /* ------------------------------------------------ DEL DIPLOMA ----------------------------------------- */
    @ApiOperation(value = "DEL DIPLOMA")
    @DeleteMapping(value = "/profil/diploma/del-diploma/{diplomaId}")
    public String delDiploma(@PathVariable Integer diplomaId) {

        String massageStatus = "";

        try {

            /** @see DiplomaRepository#deleteDiplomaById(Integer) */
            Integer check = diplomaRepository.deleteDiplomaById(diplomaId);

            if (check == 0) {
                massageStatus = "Failed deletion - diploma not found";
            } else {
                massageStatus = "Successful deletion of the diploma";
            }
        } catch (Exception pEX) {
            logger.error(String.valueOf(pEX));
        }
        return massageStatus;
    }

}
