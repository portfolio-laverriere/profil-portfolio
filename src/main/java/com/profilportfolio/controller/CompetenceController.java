package com.profilportfolio.controller;

import com.profilportfolio.exception.CompetenceConflictException;
import com.profilportfolio.exception.CompetenceNotFoundException;
import com.profilportfolio.exception.ProfilNotFoundException;
import com.profilportfolio.repository.CompetenceRepository;
import com.profilportfolio.repository.ProfilRepository;
import com.profilportfolio.service.dto.CompetenceDTO;
import com.profilportfolio.service.mapper.CompetenceMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@Api(description = "API POUR LES OPERATIONS CRUD SUR LES COMPETENCES")
@RestController
public class CompetenceController {

    private static final Logger logger = LoggerFactory.getLogger(CompetenceController.class);

    @Autowired
    private CompetenceRepository competenceRepository;

    @Autowired
    private ProfilRepository profilRepository;


                                        /* ===================================== */
                                        /* ================ GET ================ */
                                        /* ===================================== */

    /* --------------------------------------- GET COMPETENCE BY PROFIL ---------------------------------------- */
    @ApiOperation(value = "GET ALL COMPETENCE BY PROFIL")
    @GetMapping(value = "/profil/competence/get-all-competence/{profilId}")
    public List<CompetenceDTO> getAllCompetenceByProfil(@PathVariable Integer profilId) {

        List<CompetenceDTO> competenceDTOList = new ArrayList<>();

        /**
         * JE VERIFIE SI LE PROFIL EXISTE
         */
        Boolean profilExists = profilRepository.existsById(profilId);

        if (profilExists) {

            try {

                /**
                 * OBTENIR TOUTES LES COMPETENCES PAR L'ID PROFIL
                 * @see CompetenceRepository#findAllByProfilId(Integer)
                 */
                competenceDTOList = CompetenceMapper.INSTANCE.toDTOList(competenceRepository.findAllByProfilId(profilId));

            } catch (Exception pEX) {
                logger.error(String.valueOf(pEX));
            }

        } else {
            throw new ProfilNotFoundException("Profil not found for the ID : " + profilId +
                    " - Cannot retrieve competence related to the profil");
        }
        return competenceDTOList;
    }


                                        /* ===================================== */
                                        /* ================ ADD ================ */
                                        /* ===================================== */

    /* ------------------------------------------ ADD COMPETENCE ------------------------------------------ */
    @ApiOperation(value = "ADD COMPETENCE FOR PROFIL")
    @PostMapping(value = "/profil/competence/add-competence/{profilId}")
    public List<CompetenceDTO> addCompetenceForProfil(@RequestBody List<CompetenceDTO> competenceDTOList,
                                                      @PathVariable Integer profilId) {

        /**
         * JE VERIFIE SI LE PROFIL EXISTE
         */
        Boolean profilExists = profilRepository.existsById(profilId);

        if (profilExists) {

            for (CompetenceDTO competenceDTO : competenceDTOList) {

                /** J'AJOUTE A COMPETENCE L'ID DU PROFIL AUQUEL CELUI-CI EST LIE */
                competenceDTO.setProfilId(profilId);

                /** @see CompetenceRepository#competenceExistsByName(String) */
                Boolean competenceExistsByName = competenceRepository.competenceExistsByName(competenceDTO.getName());

                if (!competenceExistsByName) {

                    /** J'AJOUTE COMPETENCE, PUIS JE RECUPERE SON IDENTIFIANT */
                    competenceDTO.setId(CompetenceMapper.INSTANCE.toDTO(
                            competenceRepository.save(CompetenceMapper.INSTANCE.toEntity(competenceDTO))).getId());

                } else {
                    throw new CompetenceConflictException("The competence for the following name '" + competenceDTO.getName() +
                            "' has already been added");
                }
            }

        } else {
            throw new ProfilNotFoundException("Profil not found for the ID : " + profilId +
                    " - Cannot add competencies related to the profile");
        }
        return competenceDTOList;
    }


                                        /* ===================================== */
                                        /* ============== UPDATE =============== */
                                        /* ===================================== */

    /* -------------------------------------------- UP COMPETENCE -------------------------------------------- */
    @ApiOperation(value = "UP COMPETENCE")
    @PutMapping(value = "/profil/competence/up-competence")
    public CompetenceDTO upCompetence(@RequestBody CompetenceDTO competenceDTO) {

        /**
         * JE VERIFIE SI LA COMPETENCE EXISTE
         */
        Boolean competenceExists = competenceRepository.existsById(competenceDTO.getId());

        if (competenceExists) {

            try {

            competenceDTO = CompetenceMapper.INSTANCE.toDTO(
                    competenceRepository.save(CompetenceMapper.INSTANCE.toEntity(competenceDTO)));

            } catch (Exception pEX) {
                logger.error("{}", String.valueOf(pEX));
            }

        } else {
            logger.error("Update failure - competence : '" + competenceDTO.getName() + "' - Is not found");
            throw new CompetenceNotFoundException("Update failure - competence : '" + competenceDTO.getName() +
                    "' - Is not found");
        }
        return competenceDTO;
    }


                                        /* ======================================= */
                                        /* =============== DELETE  =============== */
                                        /* ======================================= */

    /* ------------------------------------- DEL COMPETENCE ------------------------------------- */
    @ApiOperation(value = "DEL COMETENCE")
    @DeleteMapping(value = "/profil/competence/del-competence/{competenceId}")
    public String delCompetence(@PathVariable Integer competenceId) {

        String massageStatus = "";

        try {

            /** @see CompetenceRepository#deleteCompetenceById(Integer) */
            Integer check = competenceRepository.deleteCompetenceById(competenceId);

            if (check == 0) {
                massageStatus = "Failed deletion - competence not found";
            } else {
                massageStatus = "Successful deletion of the competence";
            }
        } catch (Exception pEX) {
            logger.error(String.valueOf(pEX));
        }
        return massageStatus;
    }

}
