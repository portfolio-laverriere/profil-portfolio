package com.profilportfolio.controller;

import com.profilportfolio.exception.*;
import com.profilportfolio.proxy.MicroserviceMediaProxy;
import com.profilportfolio.repository.ConferenceRepository;
import com.profilportfolio.repository.ProfilRepository;
import com.profilportfolio.service.dto.ConferenceDTO;
import com.profilportfolio.service.mapper.ConferenceMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@Api( description = "API POUR LES OPERATIONS CRUD SUR LES CONFERENCES")
@RestController
public class ConferenceController {

    private static final Logger logger = LoggerFactory.getLogger(ConferenceController.class);

    @Autowired
    private ConferenceRepository conferenceRepository;

    @Autowired
    private ProfilRepository profilRepository;

    @Autowired
    private MicroserviceMediaProxy microserviceMediaProxy;


                                        /* ===================================== */
                                        /* ================ GET ================ */
                                        /* ===================================== */

    /* ----------------------------------- GET ALL CONFERENCE BY PROFIL ------------------------------------- */
    @ApiOperation(value = "GET ALL CONFERENCE BY PROFIL")
    @GetMapping(value = "/profil/conference/get-all-conference/{profilId}")
    public List<ConferenceDTO> getAllConferenceByProfil(@PathVariable Integer profilId) {

        List<ConferenceDTO> conferenceDTOList = new ArrayList<>();

        /**
         * JE VERIFIE SI LE PROFIL EXISTE
         */
        Boolean profilExists = profilRepository.existsById(profilId);

        if (profilExists) {

            try {

                /**
                 * OBTENIR TOUTES LES CONFERENCES PAR L'ID PROFIL
                 * @see ConferenceRepository#findAllByProfilId(Integer)
                 */
                conferenceDTOList = ConferenceMapper.INSTANCE.toDTOList(conferenceRepository.findAllByProfilId(profilId));

                for (ConferenceDTO conferenceDTO : conferenceDTOList) {

                    /**
                     * OBTENIR TOUTES LES VIDEOS PAR L'ID CONFERENCE
                     * @see MicroserviceMediaProxy#getListVideoByConference(Integer)
                     */
                    conferenceDTO.setVideoDTOList(microserviceMediaProxy.getListVideoByConference(conferenceDTO.getId()));

                    /**
                     * OBTENIR TOUTES LES IMAGES PAR L'ID CONFERENCE
                     * @see MicroserviceMediaProxy#getListImageByConference(Integer)
                     */
                    conferenceDTO.setImageDTOList(microserviceMediaProxy.getListImageByConference(conferenceDTO.getId()));
                }

            } catch (Exception pEX) {
                logger.error(String.valueOf(pEX));
            }

        } else {
            throw new ProfilNotFoundException("Profil not found for the ID : " + profilId +
                    " - Cannot retrieve conference related to the profil");
        }
        return conferenceDTOList;
    }


                                        /* ===================================== */
                                        /* ================ ADD ================ */
                                        /* ===================================== */

    /* ------------------------------------------ ADD CONFERENCE ------------------------------------------ */
    @ApiOperation(value = "ADD CONFERENCE FOR PROFIL")
    @PostMapping(value = "/profil/conference/add-conference/{profilId}")
    public List<ConferenceDTO> addConferenceForProfil(@RequestBody List<ConferenceDTO> conferenceDTOList,
                                                      @PathVariable Integer profilId) {

        /**
         * JE VERIFIE SI LE PROFIL EXISTE
         */
        Boolean profilExists = profilRepository.existsById(profilId);

        if (profilExists) {

            for (ConferenceDTO conferenceDTO : conferenceDTOList) {

                /** J'AJOUTE A CONFERENCE L'ID DU PROFIL AUQUEL CELUI-CI EST LIE */
                conferenceDTO.setProfilId(profilId);

                /** @see ConferenceRepository#conferenceExistsByName(String) */
                Boolean conferenceExistsByName = conferenceRepository.conferenceExistsByName(conferenceDTO.getName());

                if (!conferenceExistsByName) {

                    /** J'AJOUTE CONFERENCE, PUIS JE RECUPERE SON IDENTIFIANT */
                    conferenceDTO.setId(ConferenceMapper.INSTANCE.toDTO(
                            conferenceRepository.save(ConferenceMapper.INSTANCE.toEntity(conferenceDTO))).getId());

                }  else {
                    throw new ConferenceConflictException("The conference for the following name '" + conferenceDTO.getName() +
                            "' has already been added");
                }
            }

        } else {
            throw new ProfilNotFoundException("Profil not found for the ID : " + profilId +
                    " - Cannot add conference related to the profile");
        }
        return conferenceDTOList;
    }


                                        /* ===================================== */
                                        /* ============== UPDATE =============== */
                                        /* ===================================== */

    /* -------------------------------------------- UP CONFERENCE -------------------------------------------- */
    @ApiOperation(value = "UP CONFERENCE")
    @PutMapping(value = "/profil/conference/up-conference")
    public ConferenceDTO upConference(@RequestBody ConferenceDTO conferenceDTO) {

        /**
         * JE VERIFIE SI LA CONFERENCE EXISTE
         */
        boolean conferenceExists = conferenceRepository.existsById(conferenceDTO.getId());

        if (conferenceExists) {

            try {

                conferenceDTO = ConferenceMapper.INSTANCE.toDTO(conferenceRepository.save(
                        ConferenceMapper.INSTANCE.toEntity(conferenceDTO)));

            } catch (Exception pEX) {
                logger.error("{}", String.valueOf(pEX));
            }

        } else {
            logger.error("Update failure - conference : '" + conferenceDTO.getName() + "' - Is not found");
            throw new ConferenceNotFoundException("Update failure - conference : '" + conferenceDTO.getName() +
                    "' - Is not found");
        }
        return conferenceDTO;
    }


                                        /* ======================================= */
                                        /* =============== DELETE  =============== */
                                        /* ======================================= */

    /* ------------------------------------- DEL CONFERENCE ------------------------------------- */
    @ApiOperation(value = "DEL CONFERENCE")
    @DeleteMapping(value = "/profil/conference/del-conference/{conferenceId}")
    public String delConference(@PathVariable Integer conferenceId) {

        String massageStatus = "";

        try {

            /** @see ConferenceRepository#deleteConferenceById(Integer) */
            Integer check = conferenceRepository.deleteConferenceById(conferenceId);

            if (check == 0) {
                massageStatus = "Failed deletion - conference not found";
            } else {
                massageStatus = "Successful deletion of the conference";
            }
        } catch (Exception pEX) {
            logger.error(String.valueOf(pEX));
        }
        return massageStatus;
    }
}
