package com.profilportfolio.proxy;

import com.profilportfolio.service.dto.ImageDTO;
import com.profilportfolio.service.dto.VideoDTO;
import feign.Headers;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import java.util.List;

@Headers("Content-Type: application/json")
@FeignClient(name = "MICROSERVICE-MEDIA", url = "http://localhost:9102/MICROSERVICE-MEDIA")
public interface MicroserviceMediaProxy {


    /**
     * OBTENIR TOUTES LES VIDEOS PAR L'ID CONFERENCE
     * @param conferenceId
     * @return
     */
    @GetMapping(value = "/media/video/conference/get-all-video/{conferenceId}")
    List<VideoDTO> getListVideoByConference(@PathVariable("conferenceId") Integer conferenceId);

    /**
     * OBTENIR TOUTES LES IMAGES PAR L'ID CONFERENCE
     * @param conferenceId
     * @return
     */
    @GetMapping(value = "/media/image/conference/get-all-image/{conferenceId}")
    List<ImageDTO> getListImageByConference(@PathVariable("conferenceId") Integer conferenceId);

    /**
     * OBTENIR TOUTES LES IMAGES PAR L'ID DIPLOMA
     * @param diplomaId
     * @return
     */
    @GetMapping(value = "/media/image/diploma/get-all-image/{diplomaId}")
    List<ImageDTO> getListImageByDiploma(@PathVariable("diplomaId") Integer diplomaId);

    /**
     * OBTENIR TOUTES LES IMAGES PAR L'ID EXPERIENCE
     * @param experienceId
     * @return
     */
    @GetMapping(value = "/media/image/experience/get-all-image/{experienceId}")
    List<ImageDTO> getListImageByExperience(@PathVariable("experienceId") Integer experienceId);

}
