package com.profilportfolio.controller;

import com.profilportfolio.exception.ConferenceConflictException;
import com.profilportfolio.exception.ConferenceNotFoundException;
import com.profilportfolio.exception.ProfilNotFoundException;
import com.profilportfolio.model.ConferenceEntity;
import com.profilportfolio.proxy.MicroserviceMediaProxy;
import com.profilportfolio.repository.ConferenceRepository;
import com.profilportfolio.repository.ProfilRepository;
import com.profilportfolio.service.dto.ConferenceDTO;
import com.profilportfolio.service.dto.ImageDTO;
import com.profilportfolio.service.dto.VideoDTO;
import com.profilportfolio.service.mapper.ConferenceMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import java.util.ArrayList;
import java.util.List;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class ConferenceControllerTest {

    @Mock
    ConferenceRepository conferenceRepository;

    @Mock
    ProfilRepository profilRepository;

    @Mock
    MicroserviceMediaProxy microserviceMediaProxy;

    @InjectMocks
    ConferenceController conferenceController;

    @BeforeEach
    void setUp() throws Exception{
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getAllConferenceByProfil() {

        List<ConferenceDTO> conferenceDTOListMock = new ArrayList<>();
        List<ImageDTO> imageDTOListMock = new ArrayList<>();
        List<VideoDTO> videoDTOListMock = new ArrayList<>();
        Integer profilId = 7;

        ConferenceDTO conferenceDTOMock1 = ConferenceDTO.builder().id(8).name("Nom conference 1")
                .summarize("Resumé conference 1").profilId(7).build();
        ImageDTO imageDTOMock = ImageDTO.builder().id(88).name("Nom image conference 1")
                .imagePath("Path image conference 1").build();
        imageDTOListMock.add(imageDTOMock);
        VideoDTO videoDTOMock = VideoDTO.builder().id(78).title("Titre video conference 1")
                .videoUrl("URL video conference 1").build();
        videoDTOListMock.add(videoDTOMock);

        conferenceDTOMock1.setImageDTOList(imageDTOListMock);
        conferenceDTOMock1.setVideoDTOList(videoDTOListMock);
        conferenceDTOListMock.add(conferenceDTOMock1);

        ConferenceDTO conferenceDTOMock2 = ConferenceDTO.builder().id(16).name("Nom conference 2")
                .summarize("Resumé conference 2").profilId(7).build();
        conferenceDTOListMock.add(conferenceDTOMock2);

        when(profilRepository.existsById(profilId)).thenReturn(true);
        when(conferenceRepository.findAllByProfilId(profilId)).thenReturn(
                ConferenceMapper.INSTANCE.toEntityList(conferenceDTOListMock));
        when(microserviceMediaProxy.getListImageByConference(conferenceDTOMock1.getId())).thenReturn(imageDTOListMock);
        when(microserviceMediaProxy.getListVideoByConference(conferenceDTOMock1.getId())).thenReturn(videoDTOListMock);

        final List<ConferenceDTO> conferenceDTOList = conferenceController.getAllConferenceByProfil(profilId);
        Assertions.assertEquals(conferenceDTOList.size(), 2);
        Assertions.assertEquals(conferenceDTOList.get(0).getId(), 8);
        Assertions.assertEquals(conferenceDTOList.get(0).getImageDTOList().get(0).getId(), imageDTOMock.getId());
        Assertions.assertEquals(conferenceDTOList.get(0).getVideoDTOList().get(0).getId(), videoDTOMock.getId());
        Assertions.assertEquals(conferenceDTOList.get(1).getId(), 16);
        Assertions.assertTrue(conferenceDTOList.get(1).getImageDTOList().isEmpty());
        Assertions.assertTrue(conferenceDTOList.get(1).getVideoDTOList().isEmpty());

        when(profilRepository.existsById(profilId)).thenReturn(false);
        Assertions.assertThrows(ProfilNotFoundException.class, () -> {
            conferenceController.getAllConferenceByProfil(profilId);
        });
    }

    @Test
    void addConferenceForProfil() {

        List<ConferenceDTO> conferenceDTOListMock = new ArrayList<>();
        Integer profilId = 7;

        ConferenceDTO conferenceDTOMock1 = ConferenceDTO.builder().name("Nom conference 1")
                .summarize("Resumé conference 1").profilId(7).build();
        conferenceDTOListMock.add(conferenceDTOMock1);

        ConferenceEntity conferenceEntityMock1 = ConferenceEntity.builder().id(13).name("Nom conference 1")
                .summarize("Resumé conference 1").profilId(7).build();

        when(profilRepository.existsById(profilId)).thenReturn(true);
        when(conferenceRepository.conferenceExistsByName(conferenceDTOMock1.getName())).thenReturn(false);
        when(conferenceRepository.save(any())).thenReturn(conferenceEntityMock1);

        final List<ConferenceDTO> conferenceDTOList = conferenceController.addConferenceForProfil(conferenceDTOListMock, profilId);
        Assertions.assertEquals(conferenceDTOList.size(), 1);
        Assertions.assertEquals(conferenceDTOList.get(0).getProfilId(), 7);
        Assertions.assertEquals(conferenceDTOList.get(0).getId(), 13);

        when(profilRepository.existsById(profilId)).thenReturn(false);
        Assertions.assertThrows(ProfilNotFoundException.class, () -> {
            conferenceController.addConferenceForProfil(conferenceDTOList, profilId);
        });

        when(profilRepository.existsById(profilId)).thenReturn(true);
        when(conferenceRepository.conferenceExistsByName(conferenceDTOMock1.getName())).thenReturn(true);
        Assertions.assertThrows(ConferenceConflictException.class, () -> {
            conferenceController.addConferenceForProfil(conferenceDTOListMock, profilId);
        });
    }

    @Test
    void upConference() {

        ConferenceDTO conferenceDTOMock = ConferenceDTO.builder().id(13).name("Nom conference 1")
                .summarize("Resumé conference 1").profilId(7).build();

        ConferenceEntity conferenceEntityMock = ConferenceEntity.builder().id(13).name("Nom conference 1 - mise à jour")
                .summarize("Resumé conference 1").profilId(7).build();

        when(conferenceRepository.existsById(conferenceDTOMock.getId())).thenReturn(true);
        when(conferenceRepository.save(any())).thenReturn(conferenceEntityMock);

        final ConferenceDTO conferenceDTO = conferenceController.upConference(conferenceDTOMock);
        Assertions.assertNotEquals(conferenceDTO.getName(), conferenceDTOMock.getName());
        Assertions.assertEquals(conferenceEntityMock.getName(), conferenceDTO.getName());

        when(conferenceRepository.existsById(conferenceDTO.getId())).thenReturn(false);
        Assertions.assertThrows(ConferenceNotFoundException.class, () -> {
            conferenceController.upConference(conferenceDTOMock);
        });
    }

    @Test
    void delConference() {

        Integer conferenceId = 32;

        when(conferenceRepository.deleteConferenceById(conferenceId)).thenReturn(1);
        final String checkStatusSuccess = conferenceController.delConference(conferenceId);
        Assertions.assertEquals(checkStatusSuccess, "Successful deletion of the conference");

        when(conferenceRepository.deleteConferenceById(conferenceId)).thenReturn(0);
        final String checkStatusFailure = conferenceController.delConference(conferenceId);
        Assertions.assertEquals(checkStatusFailure, "Failed deletion - conference not found");
    }
}
