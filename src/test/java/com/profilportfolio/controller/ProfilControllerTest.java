package com.profilportfolio.controller;

import com.profilportfolio.exception.ProfilConflictException;
import com.profilportfolio.exception.ProfilNotFoundException;
import com.profilportfolio.model.*;
import com.profilportfolio.proxy.MicroserviceMediaProxy;
import com.profilportfolio.repository.*;
import com.profilportfolio.service.dto.ImageDTO;
import com.profilportfolio.service.dto.ProfilDTO;
import com.profilportfolio.service.dto.VideoDTO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class ProfilControllerTest {

    @Mock
    private ProfilRepository profilRepository;

    @Mock
    private CompetenceRepository competenceRepository;

    @Mock
    private ConferenceRepository conferenceRepository;

    @Mock
    private DiplomaRepository diplomaRepository;

    @Mock
    private ExperienceRepository experienceRepository;

    @Mock
    private PointOfInterestRepository pointOfInterestRepository;

    @Mock
    private SocialNetworkRepository socialNetworkRepository;

    @Mock
    private TagRepository tagRepository;

    @Mock
    private MicroserviceMediaProxy microserviceMediaProxy;

    @InjectMocks
    ProfilController profilController;

    @BeforeEach
    void setUp() throws Exception{
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getProfil() throws ParseException {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        List<ProfilEntity> profilEntityListMock = new ArrayList<>();

        ProfilEntity profilEntityMock = ProfilEntity.builder().id(7).lastName("Mon profil").firstName("Prénom profil")
                .adress("Adresse profil").dateOfBirth(new Timestamp(dateFormat.parse("1983-04-14").getTime()))
                .phoneNumber("06 25 89 45 89").email("Email profil").build();
        profilEntityListMock.add(profilEntityMock);

        when(profilRepository.findAll()).thenReturn(profilEntityListMock);
        final ProfilDTO profilDTO = profilController.getProfil();
        Assertions.assertEquals(profilDTO.getId(), profilEntityMock.getId());
    }

    @Test
    void getFullProfil() throws ParseException {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        List<ProfilEntity> profilEntityListMock = new ArrayList<>();

        List<CompetenceEntity> competenceEntityListMock = new ArrayList<>();

        List<ConferenceEntity> conferenceEntityListMock = new ArrayList<>();
        List<VideoDTO> videoConferenceDTOListMock = new ArrayList<>();
        List<ImageDTO> imageConferenceDTOListMock = new ArrayList<>();

        List<DiplomaEntity> diplomaEntityListMock = new ArrayList<>();
        List<ImageDTO> imageDiplomaDTOListMock = new ArrayList<>();

        List<ExperienceEntity> experienceEntityListMock = new ArrayList<>();
        List<ImageDTO> imageExperienceDTOListMock = new ArrayList<>();

        List<PointOfInterestEntity> pointOfInterestEntityListMock = new ArrayList<>();
        List<TagEntity> tagPointOfInterestEntityListMock = new ArrayList<>();

        List<SocialNetworkEntity> socialNetworkEntityListMock = new ArrayList<>();

        /* --------------------------------------- PROFIL ---------------------------------------- */
        ProfilEntity profilEntityMock = ProfilEntity.builder().id(7).lastName("Mon profil").firstName("Prénom profil")
                .adress("Adresse profil").dateOfBirth(new Timestamp(dateFormat.parse("1983-04-14").getTime()))
                .phoneNumber("06 25 89 45 89").email("Email profil").build();
        profilEntityListMock.add(profilEntityMock);

        /* --------------------------------------- COMPETENCE ---------------------------------------- */
        CompetenceEntity competenceEntityMock = CompetenceEntity.builder().id(8).name("Nom compétence 1").numberOfPracticeMonths(12)
                .level(55).profilId(7).build();
        competenceEntityListMock.add(competenceEntityMock);

        /* --------------------------------------- CONFERENCE ---------------------------------------- */
        ConferenceEntity conferenceEntityMock = ConferenceEntity.builder().id(6).name("Nom conference").summarize("Resumer conference")
                .profilId(7).build();
        conferenceEntityListMock.add(conferenceEntityMock);

        /* --------------------------------------- VIDEO / CONFERENCE ---------------------------------------- */
        VideoDTO videoConferenceDTOMock = VideoDTO.builder().id(12).title("Titre video conference").videoUrl("Url video conference").build();
        videoConferenceDTOListMock.add(videoConferenceDTOMock);

        /* --------------------------------------- IMAGE / CONFERENCE ---------------------------------------- */
        ImageDTO imageConferenceDTOMock = ImageDTO.builder().id(19).name("Nom image conference").imagePath("Path image conference").build();
        imageConferenceDTOListMock.add(imageConferenceDTOMock);

        /* --------------------------------------- DIPLOMA ---------------------------------------- */
        DiplomaEntity diplomaEntityMock = DiplomaEntity.builder().id(55).title("Titre diploma 1").city("City diploma 1")
                .school("School diploma 1").obtaining(new Timestamp(dateFormat.parse("2015-07-01").getTime()))
                .paragraph("Paragraph diploma 1").profilId(7).build();
        diplomaEntityListMock.add(diplomaEntityMock);

        /* --------------------------------------- IMAGE / DIPLOMA ---------------------------------------- */
        ImageDTO imageDiplomaDTOMock = ImageDTO.builder().id(45).name("Nom image diploma 1").imagePath("Path image diploma 1").build();
        imageDiplomaDTOListMock.add(imageDiplomaDTOMock);

        /* --------------------------------------- EXPERIENCE ---------------------------------------- */
        ExperienceEntity experienceEntityMock = ExperienceEntity.builder().id(17).title("Titre experience 1")
                .company("Company experience 1").commencementDate(new Timestamp(dateFormat.parse("2010-05-06").getTime()))
                .stopDate(new Timestamp(dateFormat.parse("2011-05-06").getTime())).city("City experience 1")
                .paragraph("Paragraphe experience 1").profilId(7).build();
        experienceEntityListMock.add(experienceEntityMock);

        /* --------------------------------------- IMAGE / EXPERIENCE ---------------------------------------- */
        ImageDTO imageExperienceDTOMock = ImageDTO.builder().id(45).name("Nom image experience 1").imagePath("Path image experience 1").build();
        imageExperienceDTOListMock.add(imageExperienceDTOMock);

        /* --------------------------------------- POINT OF INTEREST ---------------------------------------- */
        PointOfInterestEntity pointOfInterestEntityMock = PointOfInterestEntity.builder().id(8).name("Nom Point Of Interest")
                .paragraph("Paragraphe Point Of Interest").profilId(7).build();
        pointOfInterestEntityListMock.add(pointOfInterestEntityMock);

        /* --------------------------------------- TAG / POINT OF INTEREST ---------------------------------------- */
        TagEntity tagPointOfInterestEntityMock = TagEntity.builder().id(8).name("Nom tag 1").tagPath("Path tag 1").build();
        tagPointOfInterestEntityListMock.add(tagPointOfInterestEntityMock);

        /* --------------------------------------- SOCIAL NETWORK ---------------------------------------- */
        SocialNetworkEntity socialNetworkEntityMock = SocialNetworkEntity.builder().id(8).name("Nom social network 1")
                .url("URL social network 1").profilId(7).build();
        socialNetworkEntityListMock.add(socialNetworkEntityMock);

        when(profilRepository.findAll()).thenReturn(profilEntityListMock);
        when(competenceRepository.findAllByProfilId(profilEntityMock.getId())).thenReturn(competenceEntityListMock);

        when(conferenceRepository.findAllByProfilId(profilEntityMock.getId())).thenReturn(conferenceEntityListMock);
        when(microserviceMediaProxy.getListVideoByConference(conferenceEntityMock.getId())).thenReturn(videoConferenceDTOListMock);
        when(microserviceMediaProxy.getListImageByConference(conferenceEntityMock.getId())).thenReturn(imageConferenceDTOListMock);

        when(diplomaRepository.findAllByProfilId(profilEntityMock.getId())).thenReturn(diplomaEntityListMock);
        when(microserviceMediaProxy.getListImageByDiploma(diplomaEntityMock.getId())).thenReturn(imageDiplomaDTOListMock);

        when(experienceRepository.findAllByProfilId(profilEntityMock.getId())).thenReturn(experienceEntityListMock);
        when(microserviceMediaProxy.getListImageByExperience(experienceEntityMock.getId())).thenReturn(imageExperienceDTOListMock);

        when(pointOfInterestRepository.findAllByProfilId(profilEntityMock.getId())).thenReturn(pointOfInterestEntityListMock);
        when(tagRepository.findAllTagByPointOfInterest(pointOfInterestEntityMock.getId())).thenReturn(tagPointOfInterestEntityListMock);

        when(socialNetworkRepository.findAllByProfilId(profilEntityMock.getId())).thenReturn(socialNetworkEntityListMock);

        final ProfilDTO profilDTO = profilController.getFullProfil();

        Assertions.assertEquals(profilDTO.getId(), profilEntityMock.getId());
        Assertions.assertEquals(profilDTO.getCompetenceDTOList().get(0).getProfilId(), profilEntityMock.getId());

        Assertions.assertEquals(profilDTO.getConferenceDTOList().get(0).getProfilId(), profilEntityMock.getId());
        Assertions.assertEquals(profilDTO.getConferenceDTOList().get(0).getVideoDTOList().get(0).getTitle(),
                videoConferenceDTOMock.getTitle());
        Assertions.assertEquals(profilDTO.getConferenceDTOList().get(0).getImageDTOList().get(0).getName(),
                imageConferenceDTOMock.getName());

        Assertions.assertEquals(profilDTO.getDiplomaDTOList().get(0).getProfilId(), profilEntityMock.getId());
        Assertions.assertEquals(profilDTO.getDiplomaDTOList().get(0).getImageDTOList().get(0).getName(),
                imageDiplomaDTOMock.getName());

        Assertions.assertEquals(profilDTO.getExperienceDTOList().get(0).getProfilId(), profilEntityMock.getId());
        Assertions.assertEquals(profilDTO.getExperienceDTOList().get(0).getImageDTOList().get(0).getName(),
                imageExperienceDTOMock.getName());

        Assertions.assertEquals(profilDTO.getPointOfInterestDTOList().get(0).getProfilId(), profilEntityMock.getId());
        Assertions.assertEquals(profilDTO.getPointOfInterestDTOList().get(0).getTagDTOList().get(0).getName(),
                tagPointOfInterestEntityMock.getName());

        Assertions.assertEquals(profilDTO.getSocialNetworkDTOList().get(0).getProfilId(), profilEntityMock.getId());
    }

    @Test
    void addProfil() throws ParseException {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        ProfilDTO profilDTOMock = ProfilDTO.builder().lastName("Mon profil").firstName("Prénom profil")
                .adress("Adresse profil").dateOfBirth(new Timestamp(dateFormat.parse("1983-04-14").getTime()))
                .phoneNumber("06 25 89 45 89").email("Email profil").build();

        ProfilEntity profilEntityMock = ProfilEntity.builder().id(7).lastName("Mon profil").firstName("Prénom profil")
                .adress("Adresse profil").dateOfBirth(new Timestamp(dateFormat.parse("1983-04-14").getTime()))
                .phoneNumber("06 25 89 45 89").email("Email profil").build();

        when(profilRepository.profilExistsByEmail(profilDTOMock.getEmail())).thenReturn(false);
        when(profilRepository.profilExistsByPhoneNumber(profilDTOMock.getPhoneNumber())).thenReturn(false);
        when(profilRepository.save(any())).thenReturn(profilEntityMock);

        final ProfilDTO profilDTO = profilController.addProfil(profilDTOMock);
        Assertions.assertEquals(profilDTO.getId(), profilEntityMock.getId());

        when(profilRepository.profilExistsByEmail(profilDTOMock.getEmail())).thenReturn(false);
        when(profilRepository.profilExistsByPhoneNumber(profilDTOMock.getPhoneNumber())).thenReturn(true);
        Assertions.assertThrows(ProfilConflictException.class, () -> {
            profilController.addProfil(profilDTOMock);
        });

        when(profilRepository.profilExistsByEmail(profilDTOMock.getEmail())).thenReturn(true);
        when(profilRepository.profilExistsByPhoneNumber(profilDTOMock.getPhoneNumber())).thenReturn(false);
        Assertions.assertThrows(ProfilConflictException.class, () -> {
            profilController.addProfil(profilDTOMock);
        });

        when(profilRepository.profilExistsByEmail(profilDTOMock.getEmail())).thenReturn(true);
        when(profilRepository.profilExistsByPhoneNumber(profilDTOMock.getPhoneNumber())).thenReturn(true);
        Assertions.assertThrows(ProfilConflictException.class, () -> {
            profilController.addProfil(profilDTOMock);
        });
    }

    @Test
    void upProfil() throws ParseException {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        ProfilDTO profilDTOMock = ProfilDTO.builder().id(7).lastName("Mon profil").firstName("Prénom profil")
                .adress("Adresse profil").dateOfBirth(new Timestamp(dateFormat.parse("1983-04-14").getTime()))
                .phoneNumber("06 25 89 45 89").email("Email profil").build();

        ProfilEntity profilEntityMock = ProfilEntity.builder().id(7).lastName("Mon profil - mise à jour").firstName("Prénom profil")
                .adress("Adresse profil").dateOfBirth(new Timestamp(dateFormat.parse("1983-04-14").getTime()))
                .phoneNumber("06 25 89 45 89").email("Email profil").build();

        when(profilRepository.existsById(profilDTOMock.getId())).thenReturn(true);
        when(profilRepository.save(any())).thenReturn(profilEntityMock);

        final ProfilDTO profilDTO = profilController.upProfil(profilDTOMock);
        Assertions.assertNotEquals(profilDTO.getLastName(), profilDTOMock.getLastName());
        Assertions.assertEquals(profilDTO.getLastName(), profilEntityMock.getLastName());

        when(profilRepository.existsById(profilDTOMock.getId())).thenReturn(false);
        Assertions.assertThrows(ProfilNotFoundException.class, () -> {
            profilController.upProfil(profilDTOMock);
        });
    }

    @Test
    void delFullProfil() {

        Integer profilId = 32;

        when(profilRepository.deleteProfilById(profilId)).thenReturn(1);
        final String checkStatusSuccess = profilController.delFullProfil(profilId);
        Assertions.assertEquals(checkStatusSuccess, "Successful deletion of the profil");

        when(profilRepository.deleteProfilById(profilId)).thenReturn(0);
        final String checkStatusFailure = profilController.delFullProfil(profilId);
        Assertions.assertEquals(checkStatusFailure, "Failed deletion - profil not found");
    }

}
